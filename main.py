from fastapi import FastAPI
from pydantic import BaseModel
from nltk import translate
from googletrans import Translator

app = FastAPI()
class googleTransModel(BaseModel):
	sourceText:str
	lang:str
 
class bleuModel(BaseModel):
	sourceText:str
	translatedSourceText:str

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post("/translate/")
async def translate_text(item: googleTransModel):
    translator = Translator(service_urls=['translate.google.com'])
    translated_text =  translator.translate(item.sourceText, dest=item.lang).text
    print(str(translated_text))
    return {'translated' : str(translated_text), 'lang': str(item.lang)}

@app.post("/bleutoken/")
async def calculate_bleu(mod : bleuModel):
    """
    This calculates the values from the source text and translated text
    """
    ref = [mod.sourceText.split()]
    cand = mod.translatedSourceText.split()
    
    bleuScore = translate.bleu_score.sentence_bleu(ref,cand,weights=(1,0,0,0))
    return {"bleuScore":str(bleuScore)}